# README #
### What is this repository for? ###
This repository contains information and documentation for anyone that wants to get started with Angular JS. It contains references and links to resources the user could use to learn the fundamentals of Angular JS.

The documentation contained outlines a 4 week guideline to learn Angular JS.
The guideline spans 4 phases, with each requiring approximately one week of effort. These phases are as follows:

* Week 1: Introduction to Angular JS and Git Source Control.
* Week 2: Guided application development of a forum styled web application using Angular JS.
* Week 3: Collaborative, Non- Guided development of a basic Angular JS based implementation.
* Week 4: Further learning diving into Object-Oriented Programming, SOLID programming principles and touch base with Typescript (a run time compiler translating Typescript code       into Java Script).


### How do I get set up? ###
* Make sure the user has Git installed on their machine. Follow the instructions here: https://git-scm.com/book/en/v2/Getting-Started-Installing-Git
* Once the user has Git installed: Open up terminal or cmd and navigate to your desired location for where you want this repository to be cloned. Example: `cd Desktop/<Insert Folder Name Here>`
* Clone the repository by pasting this in terminal `git clone https://SMS_Angular_JS@bitbucket.org/SMS_Angular_JS/angular-js-training.git`
* Once the repository is cloned add `git init` and the user is ready to start playing around in the repository.